<?php
/*
 * this file is mu-plugins/register-jobs-post-type.php
 *
 * Plugin Name: Register Jobs Post Type
 * Plugin URI: http://knok.be/
 * Description: Register a new custom post type for create jobs
 * Version: 1.0
 * Author: Knok Design
 * Author URI: http://knok.be
*/

if ( !post_type_exists( 'job' ) ) {
  function register_job_type() {
    register_post_type('job', [
      'label'         => 'Jobs',
      'labels'        => [
        'all_items' => 'Toutes les offres d’emplois en base de donnée',
        'singular_name' => 'job',
        'add_new_item'  => 'Ajouter une offre d’emploi',
        'add_new'  => 'Ajouter une nouvelle offre d’emploi',
      ],
      'hierarchical'    => true,
      'description'   => 'Permet d’ajouter des offres d’emplois',
      'public'        => true,
      'menu_icon'     => 'dashicons-megaphone',
      'rewrite' => [
        'slug'       => 'jobs',
        'with_front' => false,
      ],
      'supports' => ['title', 'thumbnail', 'custom-fields']
    ]);
  }
  
  add_action('init', 'register_job_type');
  
}
