<?php
/*
 * this file is mu-plugins/register-customers-post-type.php
 *
 * Plugin Name: Register Customer Post Type
 * Plugin URI: http://knok.be/
 * Description: Register a new custom post type for create customers
 * Version: 1.0
 * Author: Knok Design
 * Author URI: http://knok.be
*/

if ( !post_type_exists( 'customer' ) ) {
  function register_customer_type() {
    register_post_type('customer', [
      'label'         => 'Études de cas',
      'labels'        => [
        'all_items' => 'Toutes les clients en base de donnée',
        'singular_name' => 'client',
        'add_new_item'  => 'Ajouter une étude de cas',
        'add_new'  => 'Ajouter une nouvelle étude de cas',
      ],
      'hierarchical'    => true,
      'description'   => 'Permet d’ajouter des études de cas',
      'public'        => true,
      'menu_icon'     => 'dashicons-groups',
      'rewrite' => [
        'slug'       => 'clients',
        'with_front' => false,
      ],
      'supports' => ['title', 'thumbnail', 'custom-fields'],
      'taxonomies'  => array( 'category' ),
    ]);
  }
  
  add_action('init', 'register_customer_type');
  
}
