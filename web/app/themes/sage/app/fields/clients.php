<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('gestion_achat_energie', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('page_template', '==', 'views/customer.blade.php');

$page
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Contenu', ['placement' => 'left'])
    ->addRepeater('repeater', [
      'label' => 'Contenu',
      'required' => 0,
      'layout' => 'table',
      'button_label' => 'Ajouter du contenu',
    ])
      ->addText('title', ['label' => 'Titre à gauche', 'required' => 1])
      ->addWysiwyg('content', ['label' => 'Contenu à droite', 'required' => 1])
    ->endRepeater()
  ->addTab('Nos references', ['placement' => 'left'])
    ->addRepeater('repeater-ref', [
        'label' => 'Clients',
        'required' => 0,
        'layout' => 'table',
        'button_label' => 'Ajouter un client',
    ])
        ->addUrl('reference_link_1', ['label' => 'Lien vers le site web', 'required' => '0'])
        ->addImage('reference_image_1', ['label' => 'Image', 'required' => '1'])
    ->endRepeater()

;
return $page;
