<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('contact', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('page_template', '==', 'views/contact.blade.php');

$page
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Contenu', ['placement' => 'left'])
    ->addWysiwyg('contact_content', ['label' => 'Texte de contenu'])
  ->addTab('Encart "Rejoignez-nous"', ['placement' => 'left'])
    ->addText('contact_encart_texte', ['label' => 'Contenu de l’encart'])
    ->addFields(get_field_partial('components.button_with_icon'));
return $page;
