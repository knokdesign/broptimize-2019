<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('frontpage', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'permalink',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('page_template', '==', 'views/frontPage.blade.php');

$page
  ->addFields(get_field_partial('partials.header_slider'))
  ->addTab('Introduction', ['placement' => 'left'])
  ->addText('hAbout_quote', ['label' => 'Texte de la citation'])
  ->addWysiwyg('hAbout__text', ['label' => 'Contenu de l’introduction'])
  ->addTab('Solutions', ['placement' => 'left'])
  ->addWysiwyg('hSolutions__intro', ['label' => 'Texte d’introduction pour les solutions'])
  ->addText('hSolutions__achatenergie', ['label' => 'Texte - Gestion d’achat d’énergies'])
  ->addLink('link_achatenergie', ['label' => 'Configurer le lien vers Gestion d’achat d’energie'])
  ->addText('hSolutions__optim', ['label' => 'Texte - Optimisation des consommations'])
  ->addLink('link_optim', ['label' => 'Configurer le lien vers Optimisation des consommations'])
  ->addText('hSolutions__marches', ['label' => 'Rédaction et analyse des marchés publiques'])
  ->addLink('link_marches', ['label' => 'Configurer le lien vers Rédaction et analyse des marchés publiques'])
  ->addText('hSolutions__gestionprojets', ['label' => 'Gestion de projets énergétiques et renouvelables'])
  ->addLink('link_gestionprojets', ['label' => 'Configurer le lien vers Gestion de projets énergétiques et renouvelables'])
  ->addText('hSolutions__serviceadmin', ['label' => 'Service administratif en énergie'])
  ->addLink('link_serviceadmin', ['label' => 'Configurer le lien vers Service administratif en énergie'])
  ->addFields(get_field_partial('partials.valeurs'))
  ->addTab('Vidéo', ['placement' => 'left'])
    ->addWysiwyg('hVideo__video', ['label' => 'Ajouter le code de la vidéo'])

    ;

return $page;
