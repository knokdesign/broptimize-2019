<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('about', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('page_template', '==', 'views/about.blade.php');

$page
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Section 1', ['placement' => 'left'])
    ->addWysiwyg('about_content1', ['label' => 'Texte 1'])
    ->addImage('about_image', [
      'label' => 'Insérez une image',
      'required' => 1,
    ])
  ->addTab('Section 2', ['placement' => 'left'])
    ->addText('about_section2_title', ['label' => 'Titre de la section'])
    ->addWysiwyg('about_section2_content', ['label' => 'Texte de la section'])
  ->addTab('Équipe', ['placement' => 'left'])
    ->addRepeater('teams', ['label' => 'Membres de l’équipe', 'button_label' => 'Ajouter un membre à l’équipe', /*'layout' => 'block'*/])
      ->addText('team_name', ['label' => 'Nom', 'required' => '1'])
      ->addText('team_role', ['label' => 'Rôle', 'required' => '1'])
      ->addEmail('team_email', ['label' => 'E-mail'])
      ->addText('team_phone', ['label' => 'Num. de téléphone'])
      ->addImage('team_image', ['label' => 'Photo', 'instructions' => 'Veuillez mettre une image de 316x225px'])
    ->endRepeater();
return $page;
