<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('customer', [
  'title' => 'Client',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('post_type', '==', 'customer');

$page
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Contenu', ['placement' => 'left'])
    ->addTextarea('customer_description', ['label' => 'Description du client', 'required' => '1'])
    ->addImage('customer_image', [ 'label' => 'Insérez une image', 'required' => 1 ])
    ->addWysiwyg('customer_demande', ['label' => 'La demande', 'toolbar' => 'basic', 'media_upload' => 0, 'required' => '1', 'tabs' => 'visual'])
    ->addWysiwyg('customer_avantages', ['label' => 'Les avantages', 'toolbar' => 'basic', 'media_upload' => 0, 'required' => '1', 'tabs' => 'visual'])
  ->addTab('Solutions', ['placement' => 'left'])
    ->addRepeater('solutions', ['label' => 'Liste des solutions', 'layout' => 'block', 'button_label' => 'Ajouter une solution'])
      ->addSelect('solution_type', ['label' => 'Selectionnez le type de solution', 'required' => '1', 'return_format' => 'array' ])
        ->addChoice('gae', 'Gestion d’achat d’énergies')
        ->addChoice('optcons', 'Optimisation des consommations')
        ->addChoice('redac', 'Rédaction et analyse des marchés publiques')
      ->addWysiwyg('solution_content', ['label' => 'Texte', 'toolbar' => 'basic', 'media_upload' => 0, 'required' => '1', 'tabs' => 'visual'])
      ->addFields(get_field_partial('components.button_with_icon'))
    ->endRepeater();
    ;
return $page;
