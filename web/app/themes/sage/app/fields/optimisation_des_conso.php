<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('optimisation_des_conso', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('page_template', '==', 'views/optimisation_consommations.blade.php');

$page
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Contenu', ['placement' => 'left'])
    ->addWysiwyg('contenu_1_top', ['label' => 'Texte introduction', 'required' => '1'])
    ->addWysiwyg('contenu_2_top', ['label' => 'Texte section 2', 'required' => '1'])
    ->addWysiwyg('contenu_3_top', ['label' => 'Texte section 3', 'required' => '0'])
  ->addTab('Bandeau de contact', ['placement' => 'left'])
    ->addText('bandeau_contenu', ['label' => 'Contenu du bandeau', 'required' => '1'])
    ->addImage('bandeau_image', ['label' => 'Image de fond du bandeau'])
    ->addRepeater('solutions', ['label' => 'Ajouter des boutons', 'layout' => 'table', 'button_label' => 'Ajouter un bouton'])
      ->addFields(get_field_partial('components.button_with_icon'))
    ->endRepeater()
  ->addFields(get_field_partial('partials.valeurs'))
  ->addTab('Etudes de cas', ['label' => "Études de cas", 'placement' => 'left'])
    ->addPostObject('etudes_de_cas_1', [ 'label' => 'Étude de cas 1', 'required' => 1, 'post_type' => ['customer'], 'multiple' => 0, 'return_format' => 'object', 'ui' => 1])
    ->addPostObject('etudes_de_cas_2', [ 'label' => 'Étude de cas 2', 'required' => 0, 'post_type' => ['customer'], 'multiple' => 0, 'return_format' => 'object', 'ui' => 1, 'allow_null' => 1])
    ->addPostObject('etudes_de_cas_3', [ 'label' => 'Étude de cas 3', 'required' => 0, 'post_type' => ['customer'], 'multiple' => 0, 'return_format' => 'object', 'ui' => 1, 'allow_null' => 1])
    ->addFields(get_field_partial('components.button_with_icon'));;

return $page;
