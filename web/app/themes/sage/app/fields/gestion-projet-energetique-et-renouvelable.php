<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('gestion_projet_energetique_et_renouvelable', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('page_template', '==', 'views/gestion_projet_energetique_et_renouvelable.blade.php');

$page
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Contenu', ['placement' => 'left'])
    ->addWysiwyg('contenu_1_top', ['label' => 'Texte introduction', 'required' => '1'])
    ->addWysiwyg('contenu_2_top', ['label' => 'Texte section 2', 'required' => '1'])
    ->addWysiwyg('contenu_3_top', ['label' => 'Texte section 3', 'required' => '0']);
return $page;
