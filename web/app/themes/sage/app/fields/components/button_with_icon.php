<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
  'ui' => 1,
  'wrapper' => ['width' => 50],
];

$button = new FieldsBuilder('buttonWithIcon');

$button
  ->addGroup('buttonWithIcon', ['label' => 'Bouton'])

    ->addLink('link', ['label' => 'Configurer le lien', 'wrapper' => $config->wrapper ])
    ->setInstructions('Configurer le lien (libellé et URL).')

    ->addSelect('icon', ['ui' => $config->ui, 'allow_null' => 0, 'wrapper' => $config->wrapper])
    ->addChoices(
      ['icon-plus' => 'Plus'],
      ['icon-mail' => 'Mail'],
      ['icon-phone' => 'Phone'],
      ['icon-path' => 'Broptimize logo'],
      ['icon-validate' => 'Validate'],
      ['icon-arrow-left' => 'Arrow left'],
      ['icon-arrow-right' => 'Arrow right'],
      ['icon-service-public' => 'Service public'],
      ['icon-partenaires' => 'Partenaires'],
      ['icon-entreprise-02-02' => 'Entreprises']
    )
    ->setInstructions('Selectionnez l’icone du bouton.')

  ->endGroup();

return $button;
