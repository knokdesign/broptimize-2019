<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('customers', [
  'title' => 'Clients',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$post
  ->setLocation('page_template', '==', 'views/customers.blade.php');

$post
  ->addFields(get_field_partial('partials.header'));

return $post;
