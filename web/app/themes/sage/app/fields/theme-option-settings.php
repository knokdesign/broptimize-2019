<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('theme-option-settings', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'permalink',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('options_page', '==', 'theme-option-settings');

$page
  ->addTab('Informations de contact', ['placement' => 'left'])
    ->addText('website_address', ['label' => 'Adresse postale'])
    ->addText('website_email', ['label' => 'Adresse e-mail de contact'])
    ->addText('website_phone', ['label' => 'Numéro de téléphone de contact'])
  ->addTab('Reseaux sociaux', ['label' => 'Réseaux sociaux', 'placement' => 'left'])
    ->addText('socialLinkedin', ['label' => 'URL vers votre profil Linkedin'])
    ->addText('socialFacebook', ['label' => 'URL vers votre profil Facebook'])
    ->addText('socialYoutube', ['label' => 'URL vers votre chaîne Youtube'])
  ->addTab('ReferencesList', ['label' => 'Références', 'placement' => 'left'])
    ->addRepeater('references', ['label' => 'Liste des références', 'layout' => 'table', 'button_label' => 'Ajouter une référence'])
      ->addText('reference_name', ['label' => 'Nom', 'required' => '1'])
      ->addUrl('reference_link', ['label' => 'Lien vers le site web', 'required' => '0'])
      ->addImage('reference_image', ['label' => 'Image', 'required' => '1'])
    ->endRepeater()
   ->addTab('bandeau_contact', ['label' => 'Bandeau de contact', 'placement' => 'left'])
        ->addText('bandeau_contenu', ['label' => 'Contenu du bandeau', 'required' => '1'])
        ->addImage('bandeau_image', ['label' => 'Image de fond du bandeau'])
        ->addLink('bandeau_link', ['label' => 'Configurer le lien' ]);
return $page;
