<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('solutions', [
  'title' => 'Page',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$page
  ->setLocation('page_template', '==', 'views/solutions.blade.php');

$page
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Contenu', ['placement' => 'left'])
  ->addRepeater('solutions', ['label' => 'Liste des solutions', 'layout' => 'block', 'button_label' => 'Ajouter une solution'])
    ->addText('solution_name', ['label' => 'Nom', 'required' => '1'])
    ->addWysiwyg('solution_contenu', ['label' => 'Contenu', 'required' => '1'])
    ->addImage('solution_image', ['label' => 'Image', 'instructions' => 'Veuillez mettre une image de 498x323px', 'required' => '1'])
    ->addFields(get_field_partial('components.button_with_icon'))
  ->endRepeater();
return $page;
