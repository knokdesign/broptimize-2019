<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('jobs', [
  'title' => 'Jobs',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$post
  ->setLocation('page_template', '==', 'views/jobs.blade.php');

$post
  ->addFields(get_field_partial('partials.header'))
  ->addTab('Introduction', ['placement' => 'left'])
    ->addWysiwyg('intro_content', ['label' => 'Texte de l’introduction', 'required' => '1']);
return $post;
