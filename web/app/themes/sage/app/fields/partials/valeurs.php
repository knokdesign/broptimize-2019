<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$header = new FieldsBuilder('valeurs');

$header
  ->addTab('Valeurs', ['placement' => 'left'])
    ->addGroup('valeur1', ['label' => 'Valeur 1'])
      ->addText('hSolutions__valeur1', ['label' => 'Nom de la « valeur » 1'])
      ->addTextarea('hSolutions__valeur1Text', ['label' => 'Texte de la « valeur » 1', 'rows' => '2'])
    ->endGroup()
    ->addGroup('valeur2', ['label' => 'Valeur 2'])
      ->addText('hSolutions__valeur2', ['label' => 'Nom de la « valeur » 2'])
      ->addTextarea('hSolutions__valeur2Text', ['label' => 'Texte de la « valeur » 2', 'rows' => '2'])
    ->endGroup()
      ->addGroup('valeur3', ['label' => 'Valeur 3'])
      ->addText('hSolutions__valeur3', ['label' => 'Nom de la « valeur » 3'])
      ->addTextarea('hSolutions__valeur3Text', ['label' => 'Texte de la « valeur » 3', 'rows' => '2'])
    ->endGroup()
    ->addGroup('valeur4', ['label' => 'Valeur 4'])
      ->addText('hSolutions__valeur4', ['label' => 'Nom de la « valeur » 4'])
      ->addTextarea('hSolutions__valeur4Text', ['label' => 'Texte de la « valeur » 4', 'rows' => '2'])
    ->endGroup();

return $header;
