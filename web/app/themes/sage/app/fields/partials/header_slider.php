<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$header = new FieldsBuilder('header_slider');

$header
  ->addTab('slider', ['placement' => 'left'])
  ->addRepeater('slider', [ 'label' => 'Liste des slides', 'layout' => 'row', 'button_label' => 'Ajouter un slide' ])
    ->addText('slider_title', ['label' => 'Titre'])
    ->addImage('slider_image', ['label' => 'Image'])
    ->addFields(get_field_partial('components.button_with_icon'))
  ->endRepeater();

return $header;
