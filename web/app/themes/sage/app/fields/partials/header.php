<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$header = new FieldsBuilder('header');

$header
  ->addTab('En-tete', ['label' => 'En-tête', 'placement' => 'left'])
    ->addImage('header_image', [
      'label' => 'Téléversez l’image de fond de l’en-tête',
      'instructions' => '',
      'required' => 0,
      'return_format' => 'url',
      'preview_size' => 'thumbnail',
      'library' => 'all'
    ]);

return $header;
