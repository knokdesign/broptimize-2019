<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$post = new FieldsBuilder('post', [
  'title' => 'Article',
  'hide_on_screen' => [
    'the_content',
    'excerpt',
    'discussion',
    'comments',
    'slug',
    'format',
    'featured_image',
    'categories',
    'tags',
    'send-trackabcks'
  ]
]);

$post
  ->setLocation('post_type', '==', 'post')
  ->or('post_type', '==', 'job');

$post
  ->addFields(get_field_partial('partials.header'))
    ->addTab('Image', ['label' => 'Image et texte de mise en avant', 'placement' => 'left'])
        ->addImage('image', ['label' => 'Image de l’article', 'required' => 1])
        ->addTextarea('text-news', ['label' => 'Texte de l’article', 'required' => 1])
    ->addTab('builder', ['label' => 'Builder','placement' => 'left'])
        ->addFlexibleContent('builder', ['button_label' => 'Ajouter un bloc'])
        ->addLayout('content-img', ['label' => 'Contenu avec image à gauche/droite']) // BLOC 1
            ->addRadio('background-bloc', [
                'label' => 'Fond gris en arrière plan',
                'choices' => [
                    'yes' => 'Oui',
                    'no' => 'Non'
                ],
                'default_value' => 'no',
            ])
            ->addRadio('position-img', [
                'label' => 'Position de l\'image',
                'choices' => [
                    'left' => 'Gauche',
                    'right' => 'Droite'
                ],
                'default_value' => 'left',
            ])
            ->addImage('builder-img', [
                'label' => 'Téléversez l’image de fond',
                'instructions' => '',
                'return_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'all'
            ])
            ->addText('titre')
            ->addWysiwyg('content')
        ->addLayout('columns-title', ['label' => 'Contenu en colonnes avec titre']) // BLOC 2
            ->addRadio('background-bloc', [
                'label' => 'Fond gris en arrière plan',
                'choices' => [
                    'yes' => 'Oui',
                    'no' => 'Non'
                ],
                'default_value' => 'no',
            ])
            ->addRadio('columns-center', [
                'label' => 'Centrer le contenu ?',
                'choices' => [
                    'yes' => 'Oui',
                    'no' => 'Non'
                ],
                'default_value' => 'no',
                'layout' => 'horizontal'
            ])
            ->addRepeater('column-title', ['label' => 'Ajouter une colonne', 'min' => 0,'max' => 3,
                                           'instructions' => 'Maximum 3 colonnes'])
            ->addText('titre')
            ->addWysiwyg('content')
            ->endRepeater()
        ->addLayout('content-bg', ['label' => 'Contenu avec image en arrière plan']) // BLOC 3
            ->addImage('builder-img', [
                'label' => 'Téléversez l’image de fond',
                'instructions' => '',
                'return_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'all'
            ])
            ->addWysiwyg('content')
        ->addLayout('content-link', ['label' => 'Contenu avec titre + lien']) // BLOC 4
            ->addRadio('background-bloc', [
                'label' => 'Fond gris en arrière plan',
                'choices' => [
                    'yes' => 'Oui',
                    'no' => 'Non'
                ],
                'default_value' => 'no',
            ])
            ->addText('titre')
            ->addWysiwyg('content')
            ->addLink('link')
        ->addLayout('content-img-2', ['label' => 'Image']) // BLOC 4
            ->addImage('builder-img', [
                'label' => 'Téléversez l’image de fond',
                'instructions' => '',
                'return_format' => 'object',
                'preview_size' => 'thumbnail',
                'library' => 'all'
            ])

;

return $post;
