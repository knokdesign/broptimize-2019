<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$articles = new FieldsBuilder('Articles', [
    'title'          => 'Articles',
    'hide_on_screen' => [
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'slug',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackabcks'
    ]
]);

$articles
    ->setLocation('page_type', '==', 'posts_page');

$articles
    ->addFields(get_field_partial('partials.header'));

return $articles;
