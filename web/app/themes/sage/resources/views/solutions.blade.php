{{--
  Template Name: Solutions
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		<div class="solutions">
			@fields('solutions')
			<section class="solution">
				<div class="solution__content">
					<h2 role="heading" aria-level="2" class="solution__title">@sub('solution_name')</h2>
					<div class="wysiwyg">@sub('solution_contenu')</div>
					@include('components.buttonWithIcon')
				</div>
				<figure class="solution__figure">
					@group('buttonWithIcon')
					<a href="@sub('link', 'url')">
					@endgroup
						<img src="@sub('solution_image', 'url')" width="498" height="323" alt="">
					</a>
				</figure>
			</section>
			@endfields
		</div>
	</div>

@endsection
