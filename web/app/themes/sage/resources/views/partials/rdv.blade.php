{{--
<div class="rdv" id="rdv">
	<div class="o-wrapper">
		@shortcode('[contact-form-7 id="38" title="Laisser ses coordonnées"]')
	</div>
</div>
--}}

@if(is_page(179))
@else
  <div class="bandeau"
       style="background-image: url('@option('bandeau_image', 'url')'); margin-bottom: 0;margin-top: 0;">
    <div class="o-wrapper">
      <div class="bandeau__content">
        <strong class="bandeau__texte">@option('bandeau_contenu')</strong>
        <div class="bandeau__btns">
          <a href="{{ get_field('bandeau_link', 'options')['url'] }}" class="c-btn c-btn--primary">
            <i class="icon-path"></i>
            <div class="c-btn--primary__wrapper">
              <span>{{ get_field('bandeau_link', 'options')['title'] }}</span>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
@endif


