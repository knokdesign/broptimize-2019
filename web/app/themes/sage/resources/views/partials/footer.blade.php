<footer class="footer">
	<div class="o-wrapper">
		<div class="footer__row footer__row--first">
			@svg('broptimize-logo-footer', 'logo-footer')
			<strong>Broptimize, la startup belge des énergies.</strong>
			<address>@option('website_address')</address>
			<a href="tel:@option('website_phone')" class="footer__tel"><strong>Tel.&nbsp;:</strong> @option('website_phone')
			</a>
			<div class="nav-socials">
        <a href="@option('socialFacebook')" class="nav-socials__link" target="_blank">
          @svg('facebook')
          <span class="hidden">Facebook</span>
        </a>
				<a href="@option('socialLinkedin')" target="_blank" class="nav-socials__link">
					<i class="icon icon-linkedin"></i>
					<span class="hidden">Linkedin</span>
				</a>
        <a href="@option('socialYoutube')" class="nav-socials__link">
          @svg('youtube')
          <span class="hidden">Youtube</span>
        </a>
				<a href="tel:@option('website_phone')" class="nav-socials__link">
					<i class="icon icon-phone"></i>
					<span class="hidden">Téléphone</span>
				</a>
				<a href="mailto:@option('website_email')" class="nav-socials__link">
					<i class="icon icon-mail"></i>
					<span class="hidden">Mail</span>
				</a>
			</div>
		</div>
		<div class="footer__row footer__row--second">
			<ul class="footer__nav">
				@foreach(b_get_nav_items('footer_navigation') as $item)
					<li class="footer__navItem">
						<a href="{{ $item->url }}" class="footer__navLink">{{ $item->label }}</a>
					</li>
				@endforeach
			</ul>
		</div>
		<div class="footer__row footer__newsletter">
			<strong class="footer__newsletter__title">Newsletter</strong>
			<p>Restons en contact ! Inscrivez-vous à notre newsletter en suivant le lien ci-dessous et restez informé de notre actualité.</p>
			<a href="{{ the_permalink(198) }}" class="c-newsletter" tabindex="0">
				S'inscrire à notre newsletter
			</a>
		</div>
	</div>
	<div class="subfooter">
		<div class="o-wrapper">
			<div class="subfooter__copiryght"><strong>© BROPTIMIZE <?= date('Y');?></strong>, tous droits réservés.</div>
			<a href="http://knok.be" class="subfooter__dev" target="_blank">
				<span>Site web réalisé par</span>
				<span class="hidden"> Knok Design</span>
				<svg width="14" height="18" class="knok-logo" xmlns="http://www.w3.org/2000/svg">
					<path d="M12.9 0v7.1L8 0h-.6v8.3H8V1l5 7.3h.6V0zM4.3 10.5a2 2 0 0 0-1.5-.7c-.6 0-1.1.2-1.6.7-.5.6-.6 1.2-.6 3s0 2.5.6 3.1c.5.4 1 .7 1.6.7.6 0 1.1-.3 1.5-.7.6-.6.7-1.3.7-3 0-1.9 0-2.5-.7-3.1m.5 6.5c-.5.5-1.2.8-2 .8s-1.5-.3-2-.8c-.8-.8-.8-1.6-.8-3.5s0-2.6.8-3.4c.5-.5 1.2-.9 2-.9s1.5.4 2 1c.8.7.8 1.4.8 3.3 0 2 0 2.7-.8 3.5m5.3-4.4l-2 2.4v2.3h-.6V9h.6v5.2L12.3 9h.8l-2.6 3.2 3 5h-.7zM4.8 0L.6 5.2V0H0v8.3h.6V6l2-2.4 2.7 4.7H6L3 3.2 5.6 0z"
								fill="#FEFEFE" fill-rule="evenodd" fill-opacity=".8"></path>
				</svg>
			</a>
		</div>
	</div>
</footer>
