<div class="nav-mobile">
	<a href="{{ home_url() }}" class="nav-logo nav-logo__mobile">
		@svg('broptimize-logo', 'broptimize-logo')
	</a>
	<button class="hamburger hamburger--spring-r" type="button">
		<span class="hamburger-box">
			<span class="hamburger-inner"></span>
		</span>
	</button>
</div>
<div class="nav-wrapper o-wrapper">
	<a href="{{ home_url() }}" class="nav-logo">
		@svg('broptimize-logo', 'broptimize-logo')
	</a>
	<div class="nav-container">
		<nav class="nav">
			<ul class="nav-menu">

				@foreach(b_get_nav_items('primary_navigation') as $item)
					<li class="nav-item">
						<a href="{{ $item->url }}">
							<span class="nav-item__text">{{ $item->label }}</span>
							@if( $item->children ) <i class="icon icon-arrow-left icon-dropdown"></i> @endif
						</a>
						@if( $item->children )
							<div class="sub-nav">
								<ul class="sub-nav-group">
									@foreach( $item->children as $sub )
									<li>
										<a href="{{ $sub->url }}">
											<i class="icon {{ $sub->icon }}"></i>
											<span class="sub-nav-label">{{ $sub->label }}</span>
										</a>
									</li>
									@endforeach
								</ul>
							</div>
						@endif
					</li>
				@endforeach

			</ul>
		</nav>
		<div class="nav-socials">
			<a href="@option('socialLinkedin')" class="nav-socials__link" target="_blank">
				<i class="icon icon-linkedin"></i>
				<span class="hidden">Linkedin</span>
			</a>
      <a href="@option('socialFacebook')" class="nav-socials__link" target="_blank">
        @svg('facebook')
        <span class="hidden">Facebook</span>
      </a>
      <a href="@option('socialYoutube')" class="nav-socials__link" target="_blank">
        @svg('youtube')
        <span class="hidden">Youtube</span>
      </a>
			<!--<a href="tel:@option('website_phone')" class="nav-socials__link">
				<i class="icon icon-phone"></i>
				<span class="hidden">Téléphone</span>
			</a>
			<a href="mailto:@option('website_email')" class="nav-socials__link">
				<i class="icon icon-mail"></i>
				<span class="hidden">Mail</span>
			</a>-->
		</div>
	</div>
</div>
