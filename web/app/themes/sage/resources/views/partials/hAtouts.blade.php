<section class="hAtouts o-wrapper">
	<h2 role="heading" aria-level="2" class="hAtouts__title title title--36 title--center">Les avantages Broptimize</h2>
	<ul class="hAtouts__list">
		<li class="hAtouts__item">
			@group('valeur1')
			<div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-1@2x.png'))">
				<span class="hAtouts__titleItem">@sub('hSolutions__valeur1')</span>
			</div>
			<div class="hAtouts__text">@sub('hSolutions__valeur1Text')</div>
			@endgroup
		</li>
		<li class="hAtouts__item">
			@group('valeur2')
			<div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-2@2x.png'))">
				<span class="hAtouts__titleItem">@sub('hSolutions__valeur2')</span>
			</div>
			<div class="hAtouts__text">@sub('hSolutions__valeur2Text')</div>
			@endgroup
		</li>
		<li class="hAtouts__item">
			@group('valeur3')
			<div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-3@2x.png'))">
				<span class="hAtouts__titleItem">@sub('hSolutions__valeur3')</span>
			</div>
			<div class="hAtouts__text">@sub('hSolutions__valeur3Text')</div>
			@endgroup
		</li>
		<li class="hAtouts__item">
			@group('valeur4')
			<div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-4@2x.png'))">
				<span class="hAtouts__titleItem">@sub('hSolutions__valeur4')</span>
			</div>
			<div class="hAtouts__text">@sub('hSolutions__valeur4Text')</div>
			@endgroup
		</li>
	</ul>
</section>
