<a href="{{ the_permalink() }}" class="otherCustomers__card">
	<figure class="otherCustomers__figure" style="background-image: url('<?= get_field('customer_image')['sizes']['team-customer'] ;?>')">
		<img src="<?= get_field('customer_image')['sizes']['team-customer'] ;?>" width="498" height="432" alt="" class="hidden">
	</figure>
	<figcaption class="otherCustomers__content">
		<strong class="otherCustomers__title">{{ the_title() }}</strong>
		<span class="otherCustomers__category">@php $cat = get_the_category(); echo $cat[0]->cat_name; @endphp</span>
   <?//= str_limit(get_field('customer_demande'))  ;?>
	</figcaption>
</a>
