<section class="references__block">
	<h2 role="heading" aria-level="2" class="title title--36 title--center">NOS RÉFÉRENCES</h2>

	<div class="references o-wrapper">
		<div class="references__wrapper" id="slider-reference">

			@options('references')
			<div class="references__slider">
				<figure class="references__figure">
					<img src="<?= get_sub_field('reference_image')['sizes']['reference'] ;?>" width="" height="" alt="" title="@sub('reference_name')" class="references__img">
					<a href="@sub('reference_link')" target="_blank" class="references__link"><span class="hidden">Vers le site de @sub('reference_name')</span></a>
				</figure>
			</div>
			@endoptions
		</div>

		<div class="references__buttons o-wrapper">
			<button class="slider-home__button slider-references__button--left">
				<i class="icon icon-arrow-left"></i>
				<span class="hidden">Précédent</span>
			</button>
			<button class="slider-home__button slider-home__button--right slider-references__button--right">
				<i class="icon icon-arrow-right"></i>
				<span class="hidden">Suivant</span>
			</button>
		</div>

	</div>
</section>
