<header class="header header-page"
        @if(is_home())
          @if(get_field('header_image')) style="background-image: url('<?= the_field('header_image',133)['url'];?>')"> @endif
        @else
          @if(get_field('header_image')) style="background-image: url('<?= the_field('header_image')['url'];?>')"> @endif
        @endif

    @if( is_singular('customer') )
      <h1 role="heading" aria-level="1">Étude de cas</h1>
    @elseif( is_home() )
      <h1 role="heading" aria-level="1">{{ single_post_title() }}</h1>
    @else
      <h1 role="heading" aria-level="1">
        <?php the_title(); ?>
      </h1>
    @endif
  <!-- Navigation -->
    @include('partials.navigation')
</header>
