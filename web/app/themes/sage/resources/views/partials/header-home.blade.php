<header class="header header-home">
	<h1 role="heading" aria-level="1" class="hidden"><?php the_title(); ?></h1>

	<div class="slider-home__wrapper">
	<!-- Slider -->
	<div class="slider-home" id="slider-home">

		@fields('slider')
		<div class="slider-home__item">
				<div class="slider-home__bg" data-animation-in="zoomInImage" style="background-image: url( @sub('slider_image', 'url') );"></div>
				<div class="slider-home__content o-wrapper">
					<div class="slider-home__text">
						<strong>@sub('slider_title')</strong>
						@include('components.buttonWithIcon')
					</div>
				</div>
		</div>
		@endfields

	</div>

	<div class="slider-home__buttons o-wrapper">
		<button class="slider-home__button slider-home__button--right">
			<i class="icon icon-arrow-right"></i>
			<span class="hidden">Suivant</span>
		</button>
		<button class="slider-home__button slider-home__button--left">
			<i class="icon icon-arrow-left"></i>
			<span class="hidden">Précédent</span>
		</button>
	</div>

</div>

	<!-- Navigation -->
	@include('partials.navigation')
</header>
