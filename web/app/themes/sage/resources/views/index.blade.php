@extends('layouts.app')

@section('content')

  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
  @endif

  <div class="o-wrapper">
      <div class="newsCard__wrapper">
      @while (have_posts()) @php the_post() @endphp
      <a href="{{ the_permalink() }}" class="newsCard">
        <figure class="newsCard__figure" style="background-image: url('<?= get_field('image')['sizes']['team-customer'] ;?>')">
          <img src="<?= get_field('image')['sizes']['team-customer'] ;?>" width="498" height="432" alt="" class="hidden">
        </figure>
        <figcaption class="newsCard__content">
          <strong class="newsCard__title">{{ the_title() }}</strong>
          <p class="newsCard__excerpt"><?=  strip_tags(str_limit(get_field('text-news'))) ;?></p>
        </figcaption>
      </a>
      @endwhile
    </div>
  </div>

@endsection

