{{--
  Template Name: Mailchimp
--}}


@extends('layouts.app')

@section('content')

  <div class="o-wrapper">
    <!-- Begin Mailchimp Signup Form -->
    <!--<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">-->
    <div id="mc_embed_signup">
      <form action="https://broptimize.us2.list-manage.com/subscribe/post?u=c8d4490ea29d40213ac9a7378&amp;id=03873a4685"
            method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
            target="_blank" novalidate>
        <div id="mc_embed_signup_scroll">
          <div class="contact-form__wrapper">
            <div class="contact-form__group">
              <label for="mce-FNAME">Prénom</label>
              <input type="text" value="" name="FNAME" id="mce-FNAME">
            </div>
            <div class="contact-form__group">
              <label for="mce-LNAME">Nom</label>
              <input type="text" value="" name="LNAME" id="mce-LNAME">
            </div>
          </div>
          <div class="contact-form__wrapper">
            <div class="contact-form__group">
              <label for="mce-PHONE">Numéro de téléphone </label>
              <input type="text" name="PHONE" value="" id="mce-PHONE">
            </div>
            <div class="contact-form__group">
              <label for="mce-MMERGE3">Code postal</label>
              <input type="number" name="MMERGE3" value="" id="mce-MMERGE3">
            </div>
          </div>
          <div class="contact-form__wrapper">
            <div class="contact-form__group">
              <label for="mce-EMAIL">Adresse mail <span class="asterisk">*</span></label>
              <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="contact-form__group">
              <label for="mce-GENDER">Genre</label>
              <select id="mce-GENDER" name="GENDER">
                <option value="male">Homme</option>
                <option value="female">Femme</option>
              </select>
            </div>
          </div>
          <div class="" style="display:none">
            <label for="mce-LANG">language </label>
            <input type="text" value="<?= get_locale(); ?>" name="LANG" class="" id="mce-LANG">
          </div>
          <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
          </div>
          <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
          <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                    name="b_c8d4490ea29d40213ac9a7378_03873a4685"
                                                                                    tabindex="-1" value=""></div>
          <div class="mc-field-group input-group contact-form__acceptance-2">
            <input type="checkbox" value="1" name="group[85036][1]" id="mce-group[85036]-85036-0">
            <label for="mce-group[85036]-85036-0">
              En cochant cette case, j'accepte que BROPTIMIZE m'envoie ponctuellement des
              informations sur son actualité, ses produits et ses services. Pour plus d'information sur le sujet, je
              prends bonne note que je peux consulter la <a href="https://broptimize.be/politique-de-confidentialite/">politique de confidentialité</a>.
            </label>

          </div>

          <button class="c-btn c-btn--primary" name="subscribe" id="mc-embedded-subscribe">
            <i class="icon-plus" style="border: 0.11111rem solid #ECECEC"></i>
            <div class="c-btn--primary__wrapper" style="background-color:#ECECEC">
              <span>Envoyer</span>
            </div>
          </button>

      </form>
    </div>
    <script type=‘text/javascript’ src=‘//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js’></script>
    <script type=‘text/javascript’>
		(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]=‘EMAIL’;ftypes[0]=‘email’;fnames[1]=‘FNAME’;ftypes[1]=‘text’;fnames[2]=‘LNAME’;ftypes[2]=‘text’;fnames[4]=‘PHONE’;ftypes[4]=‘phone’;fnames[6]=‘MMERGE6’;ftypes[6]=‘number’;}(jQuery));var $mcj = jQuery.noConflict(true);





    </script>
    <!--End mc_embed_signup-->
  </div>

@endsection
