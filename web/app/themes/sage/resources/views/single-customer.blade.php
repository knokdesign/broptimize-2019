@extends('layouts.app')

@section('content')

	<section>
		<div class="o-wrapper">
			<div class="customer__top">
				<div class="customer__about">
					<h2 role="heading" aria-level="2" class="title title--36 customer__name">{{ the_title() }}</h2>
					<div class="customer__description">
						@field('customer_description')
					</div>
				</div>
				<figure class="customer__figure">
					<img src="<?= get_field('customer_image')['sizes']['team-customer']; ?>" width="498" height="432" alt="">
				</figure>
			</div>
		</div>

		<div class="customer__bottom">
			<div class="o-wrapper customer__bottom__wrapper">

				<div class="customer__bottom__left">
					<section class="customer__bloc">
						<h3 role="heading" aria-level="3" class="customer__bloc__title title title--26">La demande</h3>
						<div class="wysiwyg">@field('customer_demande')</div>
					</section>
					<section class="customer__bloc">
						<h3 role="heading" aria-level="3" class="customer__bloc__title title title--26">Les avantages</h3>
						<div class="wysiwyg">@field('customer_avantages')</div>
						@include('components.buttonWithIcon')
					</section>
				</div>

				<div class="customer__bottom__right">
					<section class="customer__bloc">
						<h3 role="heading" aria-level="3" class="customer__bloc__title title title--26">La solution</h3>
						@fields('solutions')
						<div class="customer__solution">
							<strong>@sub('solution_type', 'label')</strong>
							<div class="wysiwyg">@sub('solution_content')</div>
							@include('components.buttonWithIcon')
						</div>
						@endfields()
					</section>
				</div>

			</div>
		</div>



    <?php

    //get the taxonomy terms of custom post type
    $customTaxonomyTerms = wp_get_object_terms( $post->ID, 'category', ['fields' => 'ids'] );

    //query arguments
    $args = [
      'post_type' => 'customer',
      'post_status' => 'publish',
      'posts_per_page' => 3,
      'orderby' => 'rand',
      'tax_query' => [
        [
          'taxonomy' => 'category',
          'field' => 'id',
          'terms' => $customTaxonomyTerms
        ]
      ],
      'post__not_in' => [ $post->ID ],
    ];

    //the query
    $relatedPosts = new WP_Query( $args ); ?>

		@if ($relatedPosts->have_posts())
		<div class="o-wrapper">
			<section class="otherCustomers">
				<h2 role="heading" aria-level="2" class="title title--36 title--center">Études de cas relatives</h2>
				<div class="otherCustomers__list">

				<!-- loop through query -->
						@while( $relatedPosts->have_posts() ) @php( $relatedPosts->the_post() )

							@include('partials.customer-card')

						@endwhile
					@else

          <?php wp_reset_postdata(); ?>

				</div>
			</section>
		</div>
		@endif
	</section>
@endsection
