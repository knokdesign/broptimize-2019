<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap container" role="document">
      <div class="content">
        <main class="main">
          @yield('content')
        </main>
      </div>
    </div>
    @if(is_front_page())
      @include('partials.references')
    @endif

      <div style="margin-top: 100px;">
        @include('partials.rdv')
      </div>

    @php do_action('get_footer') @endphp
    @include('partials.footer')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    @php wp_footer() @endphp
  <div class="overlay"></div>
  </body>
</html>
