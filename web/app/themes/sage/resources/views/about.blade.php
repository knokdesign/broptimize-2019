{{--
  Template Name: About
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		<section class="abSection1">
			<h2 role="heading" aria-level="2" class="hidden">Introduction</h2>
			<div class="wysiwyg">
        <div class="abSection1__img" style="background-image: url('<?= get_field('about_image')['sizes']['about-section1'] ;?>')">
          <img src="<?= get_field('about_image')['sizes']['about-section1'] ;?>" width="" height="" alt="" class="hidden">
          <img class="abSection1__img__cover"
               src="@asset('images/cover-about-section-1.png')"
               srcset="@asset('images/cover-about-section-1@2x.png') 2x" width="" height="" alt="">
        </div>
				@field('about_content1')
			</div>

		</section>
	</div>

	<div class="o-wrapper">
		<section class="abSection2">
			<h2 role="heading" aria-level="2" class="title title36">@field('about_section2_title')</h2>
			<div class="wysiwyg">
				@field('about_section2_content')
			</div>
		</section>
	</div>

	<section class="abTeam">
		<div class="o-wrapper">
			<h2 role="heading" aria-level="2" class="title title--36 title--center">Notre équipe</h2>

			<!-- Liste des membres -->
			<div class="abTeam__bottom">
				@fields('teams')
				<section class="abTeam__bottomCard">
					<figure class="abTeam__figure">
						<img src="<?= get_sub_field('team_image')['sizes']['team-membres'] ;?>" width="316" height="425" alt="Photo de @sub('team_name')" class="abTeam__img">
					</figure>
					<div class="abTeam__content">
						<h3 role="heading" aria-level="3" class="abTeam__name">@sub('team_name')</h3>
						<strong class="abTeam__role">@sub('team_role')</strong>
						@hassub('team_email')<a href="mailto:@sub('team_email')" class="abTeam__email">@sub('team_email')</a>@endsub
						@hassub('team_phone')<a href="tel:@sub('team_phone')" class="abTeam__phone">@sub('team_phone')</a>@endsub
					</div>
				</section>
				@endfields
			</div>

		</div>
	</section>

@endsection
