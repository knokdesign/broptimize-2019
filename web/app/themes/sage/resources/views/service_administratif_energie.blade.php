{{--
  Template Name: Service administratif en énergie
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		<div class="wysiwyg gae__content1">@field('contenu_1_top')</div>
		<div class="gae__section">
			<div class="wysiwyg">@field('contenu_2_top')</div>
			<ul class="keys">
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('assistance')
          </div>
          <span>Assistance lors de la reprise temporaire de compteurs d’énergie</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('magniying-2')
          </div>
          <span>Vérification et correction de factures d’énergie</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('keys-hands', 'keys-hands')
          </div>
          <span>Médiation lors de litiges avec les fournisseurs ou gestionnaires de réseau</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('calendar')
          </div>
          <span>Analyse et adaptation des acomptes mensuels</span>
        </li>
			</ul>
		</div>
    @if(get_field('contenu_3_top'))
      <div class="wysiwyg">@field('contenu_3_top')</div>
    @endif
	</div>

@endsection
