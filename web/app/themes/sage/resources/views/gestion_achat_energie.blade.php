{{--
  Template Name: Gestion d’achat d’energies
--}}


@extends('layouts.app')

@section('content')

  <div class="o-wrapper">
    <div class="wysiwyg gae__content1">@field('contenu_1_top')</div>
    <div class="gae__section">
      <div class="wysiwyg">@field('contenu_2_top')</div>
      <ul class="keys">
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('keys-eye')
          </div>
          <span>Veille constante des marchés de l’énergie</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('keys-hands', 'key-hands')
          </div>
          <span>Négociation indépendante avec les fournisseurs</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('keys-document')
          </div>
          <span>Prise en charge des changements de fournisseurs</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('bulb')
          </div>
          <span>Résolution de tout problème administratif</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('magniying-2')
          </div>
          <span>Vérification des factures</span>
        </li>
      </ul>
    </div>
    @if(get_field('contenu_3_top'))
      <div class="wysiwyg">@field('contenu_3_top')</div>
    @endif
  </div>

  {{--<div class="bandeau" style="background-image: url('@field('bandeau_image', 'url')')">
    <img class="bandeau__img1" src="@asset('images/gae-bandeau-top.png')" width="139" height="17" alt="">
    <div class="o-wrapper">
      <div class="bandeau__content">
        <strong class="bandeau__texte">@field('bandeau_contenu')</strong>
        <div class="bandeau__btns">
          @fields('solutions')
          @include('components.buttonWithIcon')
          @endfields
        </div>
      </div>
    </div>
    <img class="bandeau__img2" src="@asset('images/gae-bandeau-bottom.png')" width="291" height="71" alt="">
  </div>--}}

  {{--	@include('partials.hAtouts')--}}

  <!--<div class="o-wrapper">
    <section class="otherCustomers otherCustomers--gae">
      <h2 role="heading" aria-level="2" class="title title--36 title--center">QUELQUES EXEMPLES</h2>
      <div class="otherCustomers__list">

        <?php $post_object = get_field('etudes_de_cas_1');
  if( $post_object ): global $post; $post = $post_object; setup_postdata($post); ?>
  @include('partials.customer-card')
  <?php wp_reset_postdata(); ?>
  <?php endif; ?>

  <?php $post_object = get_field('etudes_de_cas_2');
  if( $post_object ): global $post; $post = $post_object; setup_postdata($post); ?>
  @include('partials.customer-card')
  <?php wp_reset_postdata(); ?>
  <?php endif; ?>

  <?php $post_object = get_field('etudes_de_cas_3');
  if( $post_object ): global $post; $post = $post_object; setup_postdata($post); ?>
  @include('partials.customer-card')
  <?php wp_reset_postdata(); ?>
  <?php endif; ?>

    </div>

    <div style="margin: 0 auto; text-align: center;">
@include('components.buttonWithIcon')
    </div>


  </section>
</div>-->
@endsection
