{{--
  Template Name: Etudes de cas
--}}


@extends('layouts.app')

@section('content')

	<?php
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args = [
			'post_type' => 'customer',
			'orderby' => 'date',
			'posts_per_page' => 9,
			'paged' => $paged
		];

		$the_query = new WP_Query( $args );
		;?>

	{{--<div class="o-wrapper">
		<div class="customers__wrapper">
		@while( $the_query->have_posts() ) @php( $the_query->the_post() )
			@include('partials.customer-card')
		@endwhile
		</div>
		@php( wp_reset_postdata() )
	</div>--}}

@endsection
