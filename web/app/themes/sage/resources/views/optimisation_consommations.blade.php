{{--
  Template Name: Optimisation des consommations
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		<div class="wysiwyg gae__content1">@field('contenu_1_top')</div>
		<div class="gae__section">
			<div class="wysiwyg">@field('contenu_2_top')</div>
			<ul class="keys">
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('clock', 'clock')
          </div>
          <span>Contrôle en temps réel des consommations</span>
        </li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('keys-books')
          </div>
          <span>Répartition des différents postes énergétiques</span>
        </li>
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('picto-consobatiment')
					</div>
					<span>Synthèse de la consommation énergétique du bâtiment</span>
				</li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('balance')
          </div>
          <span>Comparaison avec des bâtiments aux profils similaires</span>
        </li>
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('picto-optimiserconso')
					</div>
					<span>Définition de pistes d’optimisation</span>
				</li>
			</ul>
		</div>
    @if(get_field('contenu_3_top'))
      <div class="wysiwyg">@field('contenu_3_top')</div>
    @endif
	</div>

	<!--<div class="bandeau" style="background-image: url('@field('bandeau_image', 'url')')">
		<img class="bandeau__img1" src="@asset('images/gae-bandeau-top.png')" width="139" height="17" alt="">
		<div class="o-wrapper">
			<div class="bandeau__content">
				<strong class="bandeau__texte">@field('bandeau_contenu')</strong>
				<div class="bandeau__btns">
					@fields('solutions')
					@include('components.buttonWithIcon')
					@endfields
				</div>
			</div>
		</div>
		<img class="bandeau__img2" src="@asset('images/gae-bandeau-bottom.png')" width="291" height="71" alt="">
	</div>-->

	{{-- @include('partials.hAtouts')  --}}

	<!--<div class="o-wrapper">
		<section class="otherCustomers otherCustomers--gae">
			<h2 role="heading" aria-level="2" class="title title--36 title--center">EN PRATIQUE</h2>
			<div class="otherCustomers__list">

				@if(get_field('etudes_de_cas_1'))
        <?php $post_object = get_field('etudes_de_cas_1');
        if( $post_object ): global $post; $post = $post_object; setup_postdata( $post ); ?>
					@include('partials.customer-card')
        	<?php wp_reset_postdata(); ?>
        <?php endif; ?>
				@endif

				@if(get_field('etudes_de_cas_2'))
				<?php $post_object = get_field('etudes_de_cas_2');
					if( $post_object ): global $post; $post = $post_object; setup_postdata( $post ); ?>
					@include('partials.customer-card')
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				@endif

				@if(get_field('etudes_de_cas_3'))
				<?php $post_object = get_field('etudes_de_cas_3');
					if( $post_object ): global $post; $post = $post_object; setup_postdata( $post ); ?>
					@include('partials.customer-card')
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
				@endif

			</div>

			<div style="margin: 0 auto; text-align: center;">
				@include('components.buttonWithIcon')
			</div>


		</section>
	</div>-->
@endsection
