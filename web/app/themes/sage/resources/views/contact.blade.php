{{--
  Template Name: Contact
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		<div class="contact__top">
			<div class="wysiwyg">@field('contact_content')</div>
		</div>

		<div class="contact-form">
			@shortcode('[contact-form-7 id="91" title="Formulaire de contact"]')
		</div>
    <div class="postuler">
      <strong>@field('contact_encart_texte')</strong>
      <a href="{{ the_permalink(179) }}" class="">Envoyez-nous votre candidature</a>
    </div>
	</div>

@endsection
