@group('buttonWithIcon')
	@hassub('link')
	<a href="@sub('link', 'url')" class="c-btn c-btn--primary">
		<i class="@sub('icon')"></i>
		<div class="c-btn--primary__wrapper">
			<span>@sub('link', 'title')</span>
		</div>
	</a>
	@endsub
@endgroup
