{{--
  Template Name: redaction_analyse_marches_publiques
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		<div class="wysiwyg gae__content1">@field('contenu_1_top')</div>
		<div class="gae__section">
			<div class="wysiwyg">@field('contenu_2_top')</div>
			<ul class="keys">
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('pen-doc')
					</div>
					<span>Collecte des données relatives aux différents points de consommation</span>
				</li>
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('keys-document')
					</div>
					<span>Rédaction du cahier de charges</span>
				</li>
				<li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('magniying-2', 'magniying')
          </div>
					<span>Analyse et suivi des offres reçues des fournisseurs</span>
				</li>
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('ham-law')
					</div>
					<span>Attribution du marché public</span>
				</li>
			</ul>
		</div>
    @if(get_field('contenu_3_top'))
      <div class="wysiwyg">@field('contenu_3_top')</div>
    @endif
	</div>

@endsection
