{{--
  Template Name: Gestion de projet energetique et renouvelable
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		<div class="wysiwyg gae__content1">@field('contenu_1_top')</div>
		<div class="gae__section">
			<div class="wysiwyg">@field('contenu_2_top')</div>
			<ul class="keys">
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('picto-optimiserconso')
					</div>
					<span>Définition de pistes de solutions réduisant la consommation d’énergie</span>
				</li>
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('compass')
					</div>
					<span>Étude technique et dimensionnement du projet énergétique ou renouvelable</span>
				</li>
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('calculator')
					</div>
					<span>Calcul de rentabilité</span>
				</li>
				<li class="keys__item">
					<div class="keys__wrapperSvg">
						@svg('euro-2')
					</div>
					<span>Support et conseil pour le financement</span>
				</li>
        <li class="keys__item">
          <div class="keys__wrapperSvg">
            @svg('puzzle-2')
          </div>
          <span>Assistance dans la réalisation concrète du projet</span>
        </li>
			</ul>
		</div>
    @if(get_field('contenu_3_top'))
      <div class="wysiwyg">@field('contenu_3_top')</div>
    @endif
	</div>

@endsection
