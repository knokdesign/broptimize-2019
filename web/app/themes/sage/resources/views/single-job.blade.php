@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="article">
    @layouts('builder')

    {{--Bloc avec image--}}
    @layout('content-img')
    <div class="c-builder__content-{{ get_sub_field('background-bloc') }}">
      <div class="c-builder__content-container o-wrapper">
        <figure class="c-builder__figure-{{ get_sub_field('position-img') }}">
          <img src="<?= get_sub_field('builder-img')['sizes']['about-img'];?>" class="c-builder__img" alt="">
        </figure>
        <div class="c-builder__content">
          <h2 role="heading" aria-level="2" class="title title--28">@sub('titre')</h2>
          <div class="wysiwyg">
            @sub('content')
          </div>
        </div>
      </div>
    </div>
    @endlayout()

    {{-- Bloc en 2-3-4 colonnes avec titre --}}
    @layout('columns-title')
    <div class="c-builder__content-{{ get_sub_field('background-bloc') }}">
      <div
        class="o-wrapper c-builder__columns-title <?= get_sub_field('columns-center') == 'yes' ? 'c-builder__columns-title--center' : '';?>">
        @php($nbCol = count(get_sub_field('column-title')))
        @fields('column-title')
        <div
          class="c-builder__columns-container c-builder__col c-builder__col--{{ $nbCol }}">
          @if(get_sub_field('titre'))
            <h2 role="heading" aria-level="2"
                class="title title--28">@sub('titre')</h2>
          @else
          @endif
          <div class="wysiwyg">
            @sub('content')
          </div>
        </div>
        @endfields()
      </div>
    </div>
    @endlayout()

    {{-- Contenu avec image en arrière plan --}}
    @layout('content-bg')
    <div class="c-builder__background"
         style="background-image: url(@sub('builder-img','url'))">
      <div class="c-builder__wrapper o-wrapper">
        <div class="wysiwyg">@sub('content')</div>
      </div>
    </div>
    @endlayout()

    {{-- Contenu avec titre + lien --}}
    @layout('content-link')
    <div class="c-builder__content-{{ get_sub_field('background-bloc') }}">
      <div class="o-wrapper c-builder__content-link">
        <h2 role="heading" aria-level="2"
            class="title title--28">@sub('titre')</h2>
        <div class="wysiwyg">
          @sub('content')
        </div>
        @if(get_sub_field('link'))
          <a href="@sub('link', 'url')" class="c-btn c-btn--secondary c-btn--medium">
            <div>
              <span>@sub('link', 'title')</span>
            </div>
          </a>
        @else
        @endif
      </div>
    </div>
    @endlayout()

    {{-- Image --}}
    @layout('content-img-2')
    <div class="o-wrapper c-builder__bloc-img">
      <img src="@sub('builder-img','url')" width="" height="" alt="">
    </div>
    @endlayout()
    @endlayouts()
  </div>
  @endwhile
@endsection
