{{--
  Template Name: Jobs
--}}


@extends('layouts.app')

@section('content')

	<div class="o-wrapper">
		@hasfield('intro_content')<div class="wysiwyg jobs__intro">@field('intro_content')</div>@endfield

		@query([
			'post_type' => 'job'
		])

		<div class="newsCard__wrapper">
		@posts
		<a href="{{ the_permalink() }}" class="newsCard">
			<figure class="newsCard__figure" style="background-image: url('<?= get_field('image')['sizes']['article'] ;?>')">
				<img src="<?= get_field('image')['sizes']['article'] ;?>" width="498" height="432" alt="" class="hidden">
			</figure>
			<figcaption class="newsCard__content">
				<strong class="newsCard__title">{{ the_title() }}</strong>
				<p class="newsCard__excerpt"><?=  strip_tags(str_limit(get_field('text-news'))) ;?></p>
			</figcaption>
		</a>
		@endposts
		</div>
	</div>



@endsection
