{{--
  Template Name: FrontPage
--}}


@extends('layouts.app')

@section('content')


  <div class="hAbout o-wrapper">
    <div class="hAbout__quote">
      <span class="hAbout__quoteText">@field('hAbout_quote')</span>
    </div>
    <div class="wysiwyg">
      @field('hAbout__text')
    </div>
  </div>

  <div class="hSolutions__mainWrapper">
    <section class="hSolutions o-wrapper">
      <h2 role="heading" aria-level="2" class="hSolutions__title title title--36 title--center">Nos solutions</h2>
      <div class="wysiwyg">
        @field('hSolutions__intro')
      </div>
      <ul class="hSolutions__list">
        <li class="hSolutions__item hSolutions__item--energies">
          <div class="hSolutions__img">
            @svg('gestion-achat-energies')
          </div>
          <div class="hSolutions__content">
            <span class="hSolutions__titleItem">
              <i class="icon icon-prise"></i>
              <span>Gestion d’achats d’énergie</span>
            </span>
            <p class="hSolutions__paragraph">@field('hSolutions__achatenergie')</p>
          </div>
          <a href="<?= get_field('link_achatenergie')['url']; ?>" class="hSolutions__link">
            <span class="hidden">Lire plus sur la gestion d’achats d’énergie </span>
            <i class="icon icon-plus"></i>
          </a>
        </li>

        <li class="hSolutions__item hSolutions__item--optim">
          <div class="hSolutions__img">
            @svg('service-admin-energie')
          </div>
          <div class="hSolutions__content">
            <span class="hSolutions__titleItem hSolutions__titleItem--margin">
              <i class="icon icon-pile-dossiers"></i>
              <span>Service administratif en énergie</span>
            </span>
            <p class="hSolutions__paragraph">@field('hSolutions__serviceadmin')</p>

          </div>
          <a href="<?= get_field('link_optim')['url']; ?>" class="hSolutions__link">
            <span class="hidden">Lire plus sur la rédaction et analyse des marchés publics</span>
            <i class="icon icon-plus"></i>
          </a>
        </li>

        <li class="hSolutions__item hSolutions__item--marches">
          <div class="hSolutions__img">
            @svg('analyse-marche-publics')
          </div>
          <div class="hSolutions__content">
            <span class="hSolutions__titleItem">
              <i class="icon icon-certificat"></i>
              <span>Rédaction et analyse des marchés publics</span>
            </span>
            <p class="hSolutions__paragraph">@field('hSolutions__marches')</p>

          </div>
          <a href="<?= get_field('link_marches')['url']; ?>" class="hSolutions__link">
            <span class="hidden">Lire plus sur la rédaction et analyse des marchés publics</span>
            <i class="icon icon-plus"></i>
          </a>
        </li>
      </ul>
      <ul class="hSolutions__list__2">
        <li class="hSolutions__item hSolutions__item--optim" style="margin-right: 20px;">
          <div class="hSolutions__img">
            @svg('optimisation-des-conso')
          </div>
          <div class="hSolutions__content">
            <span class="hSolutions__titleItem">
              <i class="icon icon-dashboard"></i>
              <span>Analyse des consommations</span>
            </span>
            <p class="hSolutions__paragraph">@field('hSolutions__optim')</p>

          </div>
          <a href="<?= get_field('link_gestionprojets')['url']; ?>" class="hSolutions__link">
            <span class="hidden">Lire plus sur l’analyse des consommations</span>
            <i class="icon icon-plus"></i>
          </a>
        </li>
        <li class="hSolutions__item hSolutions__item--optim">
          <div class="hSolutions__img">
            @svg('gestion-projet-energetique-renouvelable')
          </div>
          <div class="hSolutions__content">
            <span class="hSolutions__titleItem hSolutions__titleItem--margin">
              <i class="icon icon-panneaux"></i>
              <span>Gestion de Projets énergétiques et renouvelables</span>
            </span>
            <p class="hSolutions__paragraph">@field('hSolutions__gestionprojets')</p>

          </div>
          <a href="<?= get_field('link_serviceadmin')['url']; ?>" class="hSolutions__link">
            <span class="hidden">Lire plus sur la gestion de Projets énergétiques et renouvelables</span>
            <i class="icon icon-plus"></i>
          </a>
        </li>
      </ul>
    </section>
  </div>

  <div class="hAtouts__wrapper">
    <section class="hAtouts o-wrapper">
      <h2 role="heading" aria-level="2" class="hAtouts__title title title--36 title--center">Les Valeurs Broptimize</h2>
      <ul class="hAtouts__list">
        <li class="hAtouts__item">
          @group('valeur1')
          <div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-1@2x.png'))">
            <span class="hAtouts__titleItem">@sub('hSolutions__valeur1')</span>
          </div>
          <div class="hAtouts__text">@sub('hSolutions__valeur1Text')</div>
          @endgroup
        </li>
        <li class="hAtouts__item">
          @group('valeur2')
          <div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-2@2x.png'))">
            <span class="hAtouts__titleItem">@sub('hSolutions__valeur2')</span>
          </div>
          <div class="hAtouts__text">@sub('hSolutions__valeur2Text')</div>
          @endgroup
        </li>
        <li class="hAtouts__item">
          @group('valeur3')
          <div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-3@2x.png'))">
            <span class="hAtouts__titleItem">@sub('hSolutions__valeur3')</span>
          </div>
          <div class="hAtouts__text">@sub('hSolutions__valeur3Text')</div>
          @endgroup
        </li>
        <li class="hAtouts__item">
          @group('valeur4')
          <div class="hAtouts__figure" style="background-image: url(@asset('images/atouts-4@2x.png'))">
            <span class="hAtouts__titleItem">@sub('hSolutions__valeur4')</span>
          </div>
          <div class="hAtouts__text">@sub('hSolutions__valeur4Text')</div>
          @endgroup
        </li>
      </ul>
    </section>
  </div>
  <div class="hSolutions__mainWrapper">
    <div class="hVideo o-wrapper">
      <div class="hVideo__wrapper">
        @field('hVideo__video')
      </div>
    </div>
  </div>
  <div class="hNews__wrapper o-wrapper">
    <h2 role="heading" aria-level="2" class="hAtouts__title title title--36 title--center">Découvrir le reste de
      l'actualité</h2>
    <div class="newsCard__wrapper">
      @query(['post_type' => 'post', 'posts_per_page' => '3','post_status' => 'publish'])
      @posts
      <a href="{{ the_permalink() }}" class="newsCard">
        <figure class="newsCard__figure" style="background-image: url('<?= get_field('image')['sizes']['article'];?>')">
          <img src="<?= get_field('image')['sizes']['article'];?>" width="498" height="432" alt="" class="hidden">
        </figure>
        <figcaption class="newsCard__content">
          <strong class="newsCard__title">{{ the_title() }}</strong>
          <p class="newsCard__excerpt"><?=  strip_tags(str_limit(get_field('text-news')));?></p>
        </figcaption>
      </a>
      @endposts
    </div>
    <div class="hNews__btn">
      <a href="{{ the_permalink(133) }}" class="c-btn c-btn--primary">
        <i class="@sub('icon')"></i>
        <div class="c-btn--primary__wrapper c-btn--primary__grey">
          <span>Voir toutes nos actualités</span>
        </div>
      </a>
    </div>

  </div>

@endsection
