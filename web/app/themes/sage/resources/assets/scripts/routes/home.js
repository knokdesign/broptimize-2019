export default {
  init() {
    // Init the slider on header
    $('#slider-home').slick({
      slidesToShow: 1,
      arrows: false,
      easing: 'ease',
      speed: '1800',
      lazyLoad: 'progressive',
      infinite: 'false',
      fade: true,
      autoplay: true,
      autoplaySpeed: 7000,
      dots: true,
      touchMove: true,
    }).slickAnimation();

    $('button.slider-home__button--right').click(function () {
      $('#slider-home').slick('slickNext');
    });

    $('button.slider-home__button--left').click(function () {
      $('#slider-home').slick('slickPrev');
    });

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
