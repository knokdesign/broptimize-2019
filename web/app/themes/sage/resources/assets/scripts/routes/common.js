export default {
  init() {
    // Define script for menu bar top
    if (!window.matchMedia('(max-width: 63.99em)').matches) {
      $('.nav').accessibleMegaMenu({
        uuidPrefix: 'accessible-megamenu',
        menuClass: 'nav-menu',
        topNavItemClass: 'nav-item',
        panelClass: 'sub-nav',
        panelGroupClass: 'sub-nav-group',
        hoverClass: 'hover',
        focusClass: 'focus',
        openClass: 'open',
        openOnMouseover: true,
      });
    }

    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
      $('html').toggleClass('menu-open');
      $('html').ontouchend = (e) => {
        e.preventDefault();
      }
    });

    $('.overlay').click(function () {
      $('html').removeClass('menu-open');
      $('.hamburger').removeClass('is-active');
    });


    $('#slider-reference').slick({
      slidesToShow: 7,
      arrows: false,
      easing: 'ease',
      speed: '600',
      lazyLoad: 'progressive',
      infinite: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 475,
          settings: {
            slidesToShow: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    }).slickAnimation();

    $('button.slider-references__button--right').click(function () {
      $('#slider-reference').slick('slickNext');
    });

    $('button.slider-references__button--left').click(function () {
      $('#slider-reference').slick('slickPrev');
    });


    $('#slider-reference-1').slick({
      slidesToShow: 7,
      arrows: false,
      easing: 'ease',
      speed: '600',
      lazyLoad: 'progressive',
      infinite: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 5,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
          },
        },
        {
          breakpoint: 475,
          settings: {
            slidesToShow: 1,
          },
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ],
    }).slickAnimation();

    $('button.slider-references__button--right').click(function () {
      $('#slider-reference-1').slick('slickNext');
    });

    $('button.slider-references__button--left').click(function () {
      $('#slider-reference-1').slick('slickPrev');
    });
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
